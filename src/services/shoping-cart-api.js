const ShopingCartApi = {
    fetchAllProducts: async () => {
        const res = await fetch('http://localhost:8080/api/products');
        const result = res.json();
        return result;
    },
    fetchProductById: async (productId) => {
        const res = await fetch(`http://localhost:8080/api/product/${productId}`)
        const result = await res.json()
        return result
    },
    fetchProductsBySearchQuery: async (query) => {
        const res = await fetch("http://localhost:8080/api/products")
        const result = await res.json()
        return result.filter((product) => product.title.toLowerCase().includes(query))
    },
}

export { ShopingCartApi }