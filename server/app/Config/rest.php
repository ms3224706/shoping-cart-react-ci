<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| REST API CONFIGURATION
| -------------------------------------------------------------------------
*/

$config['rest_enable_keys'] = FALSE; // Set this to TRUE to enable the keys authentication
$config['rest_valid_logins'] = ['username' => 'password']; // Valid logins. Only used if rest_enable_keys is set to TRUE
$config['rest_ip_whitelist_enabled'] = FALSE; // Set this to TRUE to enable IP whitelist
$config['rest_ip_whitelist'] = ['127.0.0.1', '::1']; // List of whitelisted IP addresses. Only used if rest_ip_whitelist_enabled is set to TRUE

// CORS configuration
$config['allowed_origins'] = '*'; // Allowed origins. Set to '*' to allow requests from any origin.
$config['allowed_methods'] = 'GET, POST, PUT, DELETE, OPTIONS'; // Allowed HTTP methods
$config['allowed_headers'] = 'Origin, Content-Type, Accept, Authorization'; // Allowed request headers
$config['allow_credentials'] = TRUE; // Set this to TRUE to allow credentials (cookies, authorization headers, etc.)
$config['max_age'] = 3600; // Max age of preflight request (in seconds). Set to 0 to disable caching.

/*
| -------------------------------------------------------------------------
| REST API PARAMETERS
| -------------------------------------------------------------------------
*/

$config['rest_realm'] = 'REST API'; // Name of the REST API realm. Used for BASIC authentication
$config['rest_auth'] = 'basic'; // Set to 'basic' for basic HTTP authentication, 'digest' for digest authentication, or 'none' for no authentication
$config['rest_limit'] = 1000; // Number of items per page for pagination
$config['rest_key_length'] = 32; // Length of API key. Used if rest_enable_keys is set to TRUE
$config['rest_default_format'] = 'json'; // Default response format for non-formatted URLs
$config['rest_status_field_name'] = 'status'; // Field name for response status
$config['rest_message_field_name'] = 'message'; // Field name for response message
$config['rest_data_field_name'] = 'data'; // Field name for response data
$config['rest_error_delimiters'] = ['<p>', '</p>']; // Error message delimiters

/* End of file rest.php */
/* Location: ./application/config/rest.php */
