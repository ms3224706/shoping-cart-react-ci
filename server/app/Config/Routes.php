<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');

//Insert Faker Data API
$routes->get('api/insertData', 'Api::insertData');

//Products APIs
$routes->get('api/products', 'Api::index');
$routes->get('api/product/(:num)', 'Api::show/$1');
$routes->post('api/products', 'Api::create');
$routes->put('api/products/(:num)', 'Api::update/$1');
$routes->delete('api/products/(:num)', 'Api::delete/$1');

