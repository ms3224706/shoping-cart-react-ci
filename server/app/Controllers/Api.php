<?php

namespace App\Controllers;

use App\Models\ProductModel;
use CodeIgniter\RESTful\ResourceController;

class Api extends ResourceController
{
    public function insertData()
    {
        $productModel = new ProductModel();
        // Fetch data from the Fake Store API
        $httpClient = \Config\Services::curlrequest();
        $response = $httpClient->request('GET', 'https://fakestoreapi.com/products');

        if ($response->getStatusCode() == 200) {
            $fakestore_data = json_decode($response->getBody(), true);
            // Insert parsed data into the database
            foreach ($fakestore_data as $product) {
                $data = [
                    'title' => $product['title'],
                    'description' => $product['description'],
                    'price' => $product['price'],
                    'image' => $product['image'],
                ];
                $productModel->insert($data);
            }
            echo 'Data inserted successfully.';
        } else {
            echo 'Failed to fetch data from the API.';
        }
    }    

    // Retrieve all products
    public function index()
    {
        $model = new ProductModel();
        $data = $model->findAll();
        return $this->response->setHeader('Access-Control-Allow-Origin','*')->setJSON($data);
    }

    // Create a new product
    public function create()
    {
        $model = new ProductModel();
        $data = $this->request->getJSON();
        if ($model->insert($data)) {
            return $this->respondCreated($data);
        } else {
            return $this->failServerError();
        }
    }

    // Show a product
    public function show($id = null)
    {
        $model = new ProductModel();
        $data = $model->find($id);
        return $this->response->setHeader('Access-Control-Allow-Origin','*')->setJSON($data);
    }


    // Update a product
    public function update($id = null)
    {
        $model = new ProductModel();
        $data = $this->request->getJSON();
        if ($model->update($id, $data)) {
            return $this->respond($data);
        } else {
            return $this->failServerError();
        }
    }

    // Delete a product
    public function delete($id = null)
    {
        $model = new ProductModel();
        if ($model->delete($id)) {
            return $this->respondDeleted(['id' => $id]);
        } else {
            return $this->failServerError();
        }
    }
}
